package net.scero.kafka.app.services;

import lombok.RequiredArgsConstructor;
import net.scero.kafka.app.streams.Messaje;
import net.scero.kafka.app.streams.StreamsExample;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeTypeUtils;

/**
 * @author jlnogueira on 30/04/2018
 */
@RequiredArgsConstructor
@Service
public class MessajeService {
	private final StreamsExample streamsExample;

	public void sendMessaje(Messaje messaje){
		MessageChannel messajeChannel = streamsExample.outboundMessaje();
		messajeChannel.send(MessageBuilder.withPayload(messaje).setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON).build());
	}
}
