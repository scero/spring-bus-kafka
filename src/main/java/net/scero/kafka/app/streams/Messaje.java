package net.scero.kafka.app.streams;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author jlnogueira on 27/04/2018
 */
@Getter
@Setter
@ToString
@Builder
public class Messaje {
		private String name;
		private int age;
}
