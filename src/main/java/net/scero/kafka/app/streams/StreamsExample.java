package net.scero.kafka.app.streams;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author jlnogueira on 27/04/2018
 */
public interface StreamsExample {
	String INPUT = "stream-in";
	String OUTPUT = "stream-out";

	@Input(INPUT)
	SubscribableChannel inboundMessaje();

	@Output(OUTPUT)
	MessageChannel outboundMessaje();
}
