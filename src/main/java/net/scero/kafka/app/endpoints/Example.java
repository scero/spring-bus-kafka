package net.scero.kafka.app.endpoints;

import lombok.RequiredArgsConstructor;
import net.scero.kafka.app.services.MessajeService;
import net.scero.kafka.app.streams.Messaje;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jlnogueira on 28/02/2018
 */
@RestController
@RequiredArgsConstructor
public class Example {
	private final MessajeService messajeService;

	@GetMapping("/messaje/{name}/{age}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void greetings(@PathVariable("name") String name, @PathVariable("age") int age) {
		messajeService.sendMessaje(Messaje.builder().name(name).age(age).build());
	}
}
