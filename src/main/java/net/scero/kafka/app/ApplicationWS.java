package net.scero.kafka.app;

import net.scero.kafka.app.streams.StreamsExample;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * Main principal de la aplicación Spring Boot
 * @author jlnogueira on 15/02/2018
 */
@SpringBootApplication
@EnableBinding(StreamsExample.class)
public class ApplicationWS {
	public static void main(String[] args){
		SpringApplication.run(ApplicationWS.class, args);
	}
}
