package net.scero.kafka.app.consumer;

import lombok.extern.slf4j.Slf4j;
import net.scero.kafka.app.streams.StreamsExample;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author jlnogueira on 30/04/2018
 */
@Component
@Slf4j
public class Consumer {
	@StreamListener(StreamsExample.INPUT)
	public void handleGreetings(@Payload Message message) {
		log.info("Received greetings: {}", message);
	}
}
